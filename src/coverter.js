function hexToRgb(hex) {
    // Regex to check for correct hexvalues
    const hexRegex = /^([0-9A-Fa-f]{6})$/;
    if (hexRegex.test(hex)) {
        const red = parseInt(hex.substring(0, 2), 16);
        const green = parseInt(hex.substring(2, 4), 16);
        const blue = parseInt(hex.substring(4, 6), 16);
        return `rgb(${red}, ${green}, ${blue})`;
    } else {
        throw new Error("Invalid hex color value");
    }
}

module.exports = hexToRgb;