const app = require("./server.js");

const PORT = 3000;
const HOST = '127.0.0.1';
app.listen(PORT, HOST, () => {
    console.log(`Server running at http://${HOST}:${PORT}`);
});