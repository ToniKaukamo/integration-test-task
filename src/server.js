const express = require("express");
const converter = require("./coverter.js");
const app = express();
//Endpoint to http://127.0.0.1:3000/
app.get('/', (req,res) => {
    res.send("Connection succsesful");
});
//Endpoint to http://127.0.0.1:3000/hex-to-rgb/
app.get('/hex-to-rgb/', (req,res) => {
    res.send(`Format for converter: 4e8bfd`);
});
//Endpoint to http://127.0.0.1:3000/hex-to-rgb/:hexValue
app.get('/hex-to-rgb/:hex', (req,res) => {
    //Parameter from route
    const hexValue = req.params.hex;
    rgb = converter(hexValue);
    res.send(`Hex: ${hexValue} converted to: ${rgb}`);
});

module.exports = app; 