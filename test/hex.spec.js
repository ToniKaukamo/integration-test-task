const expect = require("chai").expect;
const converter = require("../src/coverter.js");
describe("Testing hex-to-rgb", () => {
    it("Can convert hex to rgb", () => {
        randomHexes = ["4e8bfd",
            "a048ef",
            "51972e",
            "2ab578",
            "7716e1",
            "c82850",
            "e64f24",
            "e33bae",
            "ab464a",
            "b96752"
            ];
        correctRgb = [
            "rgb(78, 139, 253)",
            "rgb(160, 72, 239)",
            "rgb(81, 151, 46)",
            "rgb(42, 181, 120)",
            "rgb(119, 22, 225)",
            "rgb(200, 40, 80)",
            "rgb(230, 79, 36)",
            "rgb(227, 59, 174)",
            "rgb(171, 70, 74)",
            "rgb(185, 103, 82)"
        ];
        let flag = true;
        for (let index = 0; index < randomHexes.length; index++) {
            currentHex = randomHexes[index];
            if (converter(currentHex) != correctRgb[index]) {
                flag = false;
            }
            //console.log(`Type:${converter(randomHexes[index])} ${converter(randomHexes[index])}`);
            //console.log(`Type:${typeof(correctRgb[index])} ${correctRgb[index]}`);
        }
        expect(flag).to.equal(true)
    });
    it("Only accepts convertable values",() =>{
        invalidHex = "4e8bfd4";
        expect(()=> {converter(invalidHex)}).to.throw("Invalid hex color value");
    });
});