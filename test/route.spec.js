// Integration test
// It is also a REST API test
// Remember, under the same umbrella
const expect = require('chai').expect;
const http = require("http");
const app = require('../src/server');

describe("Testing REST API", () => {
    /** @type {http.Server} */
    let server;
    before((done) => {
        // start the server
        server = app.listen(3000, "127.0.0.1", () => {
            done();
        });
    });
    // These are quite large but they work like in L07 lessson
    it("Test if / responds Connection succsesful", (done) => {
        const options = {
            "method": "GET",
            "hostname": "127.0.0.1",
            "port": "3000",
            "path": "/",
            "headers": {
                "user-agent": "vscode-restclient"
            }
        };
        const req = http.request(options, function (res) {
            const chunks = [];
            res.on("data", function (chunk) {
                chunks.push(chunk);
            });
            res.on("end", function () {
                const body = Buffer.concat(chunks);
                expect(body.toString()).to.equal("Connection succsesful");
                done();
            });
        });
        req.end();
    });
    it('Test if /hex-to-rgb responds "Format for converter: 4e8bfd"', (done) => {
        const options = {
            "method": "GET",
            "hostname": "127.0.0.1",
            "port": "3000",
            "path": "/hex-to-rgb",
            "headers": {
                "user-agent": "vscode-restclient"
            }
        };
        const req = http.request(options, function (res) {
            const chunks = [];
            res.on("data", function (chunk) {
                chunks.push(chunk);
            });
            res.on("end", function () {
                const body = Buffer.concat(chunks);
                expect(body.toString()).to.equal("Format for converter: 4e8bfd");
                done();
            });
        });
        req.end();
    });
    it("Test if /hex-to-rgb/:hex responds with a correct rbg value", (done) => {
        hexValue = "a048ef";
        rbgValue = "rgb(160, 72, 239)";
        correctString = `Hex: ${hexValue} converted to: ${rbgValue}`
        const options = {
            "method": "GET",
            "hostname": "127.0.0.1",
            "port": "3000",
            "path": "/hex-to-rgb/" + hexValue,
            "headers": {
                "user-agent": "vscode-restclient"
            }
        };
        const req = http.request(options, function (res) {
            const chunks = [];
            res.on("data", function (chunk) {
                chunks.push(chunk);
            });
            res.on("end", function () {
                const body = Buffer.concat(chunks);
                expect(body.toString()).to.equal(correctString);
                done();
            });
        });
        req.end();
    });
    after(() => {
        // stop the server
        server.close();
    });
});
